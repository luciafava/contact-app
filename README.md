# vue-contact-manager

Simple Contact Manager app demonstrating Vue.js working with a REST API.

## Server

Built with:

* json-server(web server)

### Getting started

json-server --watch db.json

These instructions assume json-server is running on `localhost:3000`.

### Requirements

* node >= 8.11.2
* npm >= 6.1.0
* vue >= 3.0.0

# Client

Built with:

* Vue.js (UI framework)
* vue-router (routing)
* vue-cli (tooling)
* vue-resource (HTTP client)

### Getting started

# install dependencies
npm install

# Run dev server
npm run serve

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```