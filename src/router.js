import Vue from 'vue'
import Router from 'vue-router'
import Resource from 'vue-resource'
import Contacts from './components/Contacts'
import Add from './components/Add'
import Edit from './components/Edit'
import ContactDetails from './components/ContactDetails'

Vue.use(Router)
Vue.use(Resource)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'contacts',
      component: Contacts
    },
    {
        path: '/add',
        name: 'add',
        component: Add
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: Edit
  },
    {
      path: '/contact/:id',
      name: 'contactdetails',
      component: ContactDetails
    }
  ]
})